# 1
# import time
#
# class TrafficLight:
#     __color = 'красный'
#
#     def running(self):
#         for el in ['красный', 'желтый', 'зеленый']:
#             print(el)
#             self.__color = el
#             if el == 'красный':
#                 time.sleep(7)
#             elif el == 'желтый':
#                 time.sleep(2)
#             elif el == 'зеленый':
#                 time.sleep(4)
#
#
# a = TrafficLight()
# a.running()

# 2
# class Road:
#     _length = 0
#     _width = 0
#
#     def __init__(self, length, width):
#         self.width = width
#         self.length = length
#
#     def massR(self, m, t):
#         return self.width * self.length * m * t
#
#
# r = Road(20, 5000)
# print(r.massR(25, 5))

# 3
# class Worker:
#     name = ''
#     surname = ''
#     position = ''
#     _income = {"wage": 0.0, "bonus": 0.0}
#
#     def __init__(self, name, surname, position, wage, bonus):
#         self.name = name
#         self.surname = surname
#         self.position = position
#         self._income["wage"] = wage
#         self._income["bonus"] = bonus
#
#
# class Position(Worker):
#     def __init__(self, name, surname, position, wage, bonus):
#         super().__init__(name, surname, position, wage, bonus)
#
#     def get_full_name(self):
#         return self.name + ' ' + self.surname
#
#     def get_total_income(self):
#         return self._income["wage"] + self._income["bonus"]
#
#
# w = Position('Vasya', 'Vasiliev', 'Position', 100.0, 40.0)
# print(w.get_full_name())
# print(w.get_total_income())

#4
# class Car:
#     run = False
#     speed = 0
#     color = ''
#     name = ''
#     is_police = False
#
#     def __init__(self, name, color, speed, is_police):
#         self.name = name
#         self.color = color
#         self.speed = speed
#         self.is_police = is_police
#
#     def go(self):
#         self.run = True
#
#     def stop(self):
#         self.run = False
#         self.speed = 0
#
#     def show_speed(self):
#         if self.run is True:
#             print(self.speed)
#
#     def turn(self, direction):
#         if self.run is True:
#             self.direction = direction
#             print('To the - ' + self.direction)
#
#
# class TownCar(Car):
#     def __init__(self, name, color, speed):
#         super().__init__(name, color, speed, is_police=False)
#
#     def show_speed(self):
#         if self.speed > 60:
#             print('Превышение скорости')
#
#
# class WorkCar(Car):
#     def __init__(self, name, color, speed):
#         super().__init__(name, color, speed, is_police=False)
#
#     def show_speed(self):
#         if self.speed > 40:
#             print('Превышение скорости')
#
#
# class SportCar(Car):
#     def __init__(self, name, color, speed):
#         super().__init__(name, color, speed, is_police=False)
#
#
# class PoliceCar(Car):
#     def __init__(self, name, color, speed):
#         super().__init__(name, color, speed, is_police=True)
#
#
# c = TownCar('TownCar', 'white', 70)
# c.go()
# c.show_speed()


#5
class Stationery:
    title = ''

    def __init__(self, title):
        self.title = title

    def draw(self):
        print('Запуск отрисовки. ')


class Pen(Stationery):
    def __init__(self, title):
        super().__init__(title)

    def draw(self):
        print('Pen Запуск отрисовки. ' + self.title)


class Pencil(Stationery):
    def __init__(self, title):
        super().__init__(title)

    def draw(self):
        print('Pencil Запуск отрисовки. ' + self.title)


class Handle(Stationery):
    def __init__(self, title):
        super().__init__(title)

    def draw(self):
        print('Handle Запуск отрисовки. ' + self.title)


p = Pen('asasa')
p.draw()

pp = Pencil('aaaaqqqq')
p.draw()

h = Handle('qqqq')
h.draw()
