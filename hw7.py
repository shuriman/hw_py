# 1
# class Matrix:
#     def __init__(self, matrix):
#         self.matrix = matrix
#
#     def __add__(self, other):
#         r = 0
#         for row in self.matrix:
#             c = 0
#             for column in row:
#                 self.matrix[r][c] = other.matrix[r][c] + column
#                 c += 1
#             r += 1
#
#         return Matrix(self.matrix)
#
#     def __str__(self):
#         str = ""
#         r = 0
#         for row in self.matrix:
#             c = 0
#             for column in row:
#                 str += f"{column} "
#                 c += 1
#             r += 1
#             str += "\n"
#
#         return str
#
#
# m = Matrix([[1, 2, 3],
#             [2, 2, 4]])
#
# m2 = Matrix([[1, 2, 8],
#              [1, 3, 3]])
#
# print(m + m2)


# 2
from abc import ABC, abstractmethod


class Wear(ABC):

    @abstractmethod
    def getcoatcloth(self):
        pass

    @abstractmethod
    def getsuitcloth(self):
        pass

    def __add__(self, other):
        if self.getsuitcloth() is not None:
            return self.getsuitcloth() + other.getcoatcloth()
        else:
            return self.getcoatcloth() + other.getsuitcloth()




class Coat(Wear):
    def __init__(self, v):
        self.v = v

    def getcoatcloth(self):
        return self.v / 6.5 + 0.5

    @property
    def v(self):
        return self.__v

    @v.setter
    def v(self, v):
        if v < 10:
            self.__v = 10
        elif v > 60:
            self.__v = 60
        else:
            self.__v = v

    def getsuitcloth(self):
        pass


class Suit(Wear):
    def __init__(self, h):
        self.h = h

    def getsuitcloth(self):
        return 2 * self.h + 0.3

    @property
    def h(self):
        return self.__h

    @h.setter
    def h(self, h):
        if h < 170:
            self.__h = 170
        elif h > 220:
            self.__h = 220
        else:
            self.__h = h

    def getcoatcloth(self):
        pass


w = Coat(9)
h = Suit(110)

print(h.h)
print(h.getsuitcloth())

print(w.v)
print(w.getcoatcloth())

print(w + h)

# 3
#
# class Cell:
#     def make_order(self, row):
#         str_result = ''
#         for i in range(1, self.cells + 1):
#             str_result += "*"
#             if i % row == 0:
#                 str_result += "\n"
#         return str_result
#
#     def __init__(self, cells):
#         self.cells = cells
#
#     def __add__(self, other):
#         return Cell(self.cells + other.cells)
#
#     def __sub__(self, other):
#         if self.cells - other.cells > 0:
#             return Cell(self.cells - other.cells)
#         print('Разница < 0')
#         return Cell(0)
#
#     def __mul__(self, other):
#         return Cell(self.cells * other.cells)
#
#     def __truediv__(self, other):
#         return Cell(self.cells // other.cells)
#
#
# c = Cell(5)
# c2 = Cell(3)
#
# c3 = c + c2
# print(c3.cells)
# c3 = c2 - c
# print(c3.cells)
# c3 = c * c2
# print(c3.cells)
# c = c3 / c2
# print(c.cells)
# print(c3.make_order(4))
