# 1
# while True:
#     str = input('Введите строку: ')
#     if str == '':
#         break
#     with open("my_file1.txt", "a+") as write_f:
#         print(str, file=write_f)
# 2
# with open("my_file1.txt", "r") as write_f:
#     lines = 0
#     symbols = 0
#     for line in write_f:
#         lines += 1
#         symbols += len(line)
#         print(line, len(line))
#
#     print(lines)
#     print(symbols)

# 3
# with open("zp.txt", "r") as write_f:
#     team = 0
#     zp_all = 0
#     for line in write_f:
#         team += 1
#         my_list = line.replace('\n', '').split(' ')
#         zp = float(my_list[1])
#         if zp < 20000:
#             print(my_list[0])
#         zp_all += zp
#
#     print("Средняя величина дохода сотрудников: ", zp_all / team)
#

# 4
# with open("text4.txt", "r") as read_f:
#     for line in read_f:
#         line = line.lower().replace('one', 'один').replace('two', 'два')\
#             .replace('three', 'три').replace('four', 'четыре')
#         with open("new_text.txt", "a+") as write_f:
#             print(line, file=write_f)

# 5
# from random import randint
#
# with open("rand5.txt", "w") as write_f:
#     line = ''
#     for i in range(0, 20):
#         line += str(randint(0, 100)) + ' '
#     line = line[:-1]
#     print(line, file=write_f)
#
# with open("rand5.txt", "r") as read_f:
#     sum = 0
#     for line in read_f:
#         list = line.replace('\n', '').split(' ')
#         for el in list:
#             sum += int(el)
#
#     print(sum)

# 6
# import re
# dict = {}
# with open("text6.txt", "r", encoding="utf-8") as read_f:
#     for line in read_f:
#         list = line.split(':')
#         predmet = list[0][:-1]
#         list = list[1].split(' ')
#         sum = 0
#         for el in list:
#             replaced = re.sub('[^\d]|\s', '', el)
#             if replaced == '':
#                 continue
#             sum += int(replaced)
#         dict[predmet] = sum
#
# print(dict)

#7
import json
my_dict = {}
av_profit = {}
average_profit = 0

with open("test7.txt", "r", encoding="utf-8") as read_f:
    i = 0
    for line in read_f:
        list = line.split(' ')
        name, form, debet, credit = list
        profit = float(debet) - float(credit)
        if profit > 0:
            i += 1
            average_profit += profit
            my_dict[name] = profit
        else:
            my_dict[name] = profit
    av_profit['average_profit'] = average_profit / i

new_list = [my_dict, av_profit]
print(new_list)

with open("my_file.json", "w") as write_f:
    json.dump(new_list, write_f)