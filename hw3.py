# 1
# def my_func(val_1, val_2):
#     if val_2 == 0:
#         print('Деление на ноль!')
#         return
#     return val_1 / val_2
#
#
# print(my_func(5, 2))

# 2
# def my_func2(name, family, dob, city, email, phone):
#     return "name: "+name+" family: "+family+" dob: "+dob+" city: "+city+" email: "+email+" phone: "+phone
#
#
# print(my_func2('name', 'family', 'dob', 'city', 'email', 'phone'))

# 3
# def my_func(val_1, val_2, val_3):
#     my_list = [val_1, val_2, val_3]
#     position = 0
#     my_min = my_list[0]
#     for key, val in enumerate(my_list):
#         if val <= my_min:
#             my_min = val
#             position = key
#
#     result = 0
#     for key, val in enumerate(my_list):
#         if key == position:
#             continue
#         result += val
#
#     return result
#
#
# print(my_func(1,8,7))

# 4
# def my_func(x, y):
#     result = 1
#     for i in range(0, y):
#         result *= x
#     return result
#
#
# print(my_func(3, 4))

# 5
#Пример строки 1 2 $ или 1 2 3$ но тогда 3$ это будет как символ завершения
# def my_calc(my_list):
#     global flag
#     global result
#     for el in my_list:
#         if el == '$' or el.find('$') >= 0:
#             flag = True
#             break
#         result += int(el)
#
#     return result
#
#
# flag = False
# result = 0
# while True:
#     my_string = input('Введите строку:')
#     input_list = my_string.split(' ')
#     print("Result: ", my_calc(input_list))
#     if flag:
#         break

#6

def int_func(my_string):
    return my_string.title()


input_string = input('Введите строку:')
input_list = input_string.split(' ')

result = ''
for el in input_list:
    result += int_func(el) + " "

print(result)





