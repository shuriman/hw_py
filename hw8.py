# class MyDate:
#
#     def __init__(self, sting_date):
#         self.sdate = sting_date
#
#     @classmethod
#     def get_idate(cls, param):
#         mylist = param.split('-')
#         return {'day': int(mylist[0]), 'mon': int(mylist[1]), 'year': int(mylist[2])}
#
#     @staticmethod
#     def validate(param):
#         for key, value in param.items():
#             if key == 'day':
#                 if value > 31 or value < 0:
#                     print('Error day')
#             elif key == 'mon':
#                 if value > 12 or value < 0:
#                     print('Error month')
#             elif key == 'year':
#                 if value < 0:
#                     print('Error year')
#             else:
#                 print('Error')
#
#
#
#
# md = MyDate('10-12-2020')
#
# mydate = md.get_idate('10-12-2020')
#
# print(mydate)
#
# MyDate.validate(mydate)
# 2
# class MyError(Exception):
#     def __init__(self, txt):
#         self.txt = txt
#
# inp_data1 = input("Введите делимое: ")
# inp_data = input("Введите делитель: ")
#
# try:
#     inp_data = int(inp_data)
#     inp_data1 = int(inp_data1)
#     if inp_data == 0:
#         raise MyError("Деление на ноль!")
# except ValueError:
#     print("Вы ввели не число")
# except MyError as err:
#     print(err)
# else:
#     print(f"Все хорошо. Ваш результат: {inp_data1 / inp_data}")

# 3
#
# class MyError(Exception):
#     def __init__(self, txt):
#         self.txt = txt
#
# mylist = []
# while True:
#     inp_data = input("Введите число: ")
#     if inp_data.lower() == 'stop':
#         print(mylist)
#         break
#
#     try:
#         if inp_data.isnumeric() is False:
#             raise MyError('Вы ввели не число')
#         mylist.append(inp_data)
#     except MyError as err:
#         print(err)

# from abc import ABC, abstractmethod
#
#
# class WH:
#     status = {}
#
#     def __init__(self, depts):
#         for dept in depts:
#             self.status[dept] = []
#
#     def to_dept(self, dept, tech, count):
#         i = 0
#         if type(count) == str:
#             print("НЕкорректные данные!")
#             return
#
#         for d in self.status[dept]:
#             for k, v in d.items():
#                 if k == 'Модель' and v == tech.get_model():
#                     self.status[dept][i]['Кол-во'] += count
#                     return
#             i += 1
#
#         self.status[dept].append({"Тип": type(tech).__name__, "Модель": tech.get_model(), 'Кол-во': count})
#
#
# class Tech(ABC):
#     def __init__(self, model, paper_count, resolution):
#         self.model = model
#         self.resolution = resolution
#         self.paper_count = paper_count
#
#     def get_model(self):
#         return self.model
#
#     @abstractmethod
#     def get_param(self):
#         pass
#
#
# class Printer(Tech):
#     def __init__(self, model, paper_count, resolution, printed):
#         super().__init__(model, paper_count, resolution)
#         self.printed = printed
#
#     def get_param(self):
#         return self.printed
#
#
# class Scanner(Tech):
#     def __init__(self, model, paper_count, resolution, dpi):
#         super().__init__(model, paper_count, resolution)
#         self.dpi = dpi
#
#     def get_param(self):
#         return self.dpi
#
#
# class Copier(Tech):
#     def __init__(self, model, paper_count, resolution, copied):
#         super().__init__(model, paper_count, resolution)
#         self.copied = copied
#
#     def get_param(self):
#         return self.copied
#
#
# p = Printer('HP-140', 1000, '800*600', 100)
# s = Scanner('BP-221', 900, '1024*720', 300)
#
# print(p.model)
#
# wh = WH(['dept1', 'dept2', 'dept3'])
#
# wh.to_dept('dept1', p, 10)
# wh.to_dept('dept1', s, 11)
# print(wh.status)
# wh.to_dept('dept1', p, 20)
#
# print(wh.status)


class Complex:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __add__(self, other):
        return Complex(self.a + other.a, self.b + other.b)

    def __mul__(self, other):
        return Complex(self.a * other.a + (self.b * other.b) * -1, self.b * other.a + self.a * other.b)

    def __str__(self):
        if self.b > 0:
            if self.b == 1:
                return "z = " + str(self.a) + " i"
            return "z = "+str(self.a)+" + "+str(self.b)+"i"
        else:
            if self.b == -1:
                return "z = " + str(self.a) + " - i"
            else:
                return "z = " + str(self.a) + " - " + str(self.b) + "i"


a = Complex(1, -1)
print(a)
b = Complex(3, 6)
print(a + b)
print(a * b)