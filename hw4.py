# from sys import argv
#
# script_name, hours, hours_rate, extra = argv
# zp = float(hours) * float(hours_rate) + float(extra)
# print(zp)
#
#

# 2
# my_list = [300, 2, 12, 44, 1, 1, 4, 10, 7, 1, 78, 123, 55]
# new_list = [val for i, val in enumerate(my_list) if i != 0 and val > my_list[i-1]]
#
# print(new_list)

# 3
# new_list = [el for el in range(20, 241) if el % 20 == 0 or el % 21 == 0]
#
# print(new_list)

# 4
# my_list = [2, 2, 2, 7, 23, 1, 44, 44, 3, 2, 10, 7, 4, 11]
#
# new_list = [el for el in my_list if my_list.count(el) == 1]
# print(new_list)

# 5
# from functools import reduce
#
#
# def my_func(prev_el, el):
#     return prev_el * el
#
#
# new_list = [el for el in range(100, 1001) if el % 2 == 0]
# print(reduce(my_func, new_list))

#6
# from itertools import cycle
# from itertools import count
#
# for el in count(3):
#     if el > 10:
#         break
#     else:
#         print(el)
#
# progr_lang = ["python", "java", "perl", "javascript"]
#
# с = 0
# for el in cycle(progr_lang):
#     if с > 10:
#         break
#     print(el)
#     с += 1

#7
def fact(n):
    r = 1
    for el in range(1, n + 1):
        r *= el
        yield r


new_list = [el for el in fact(4)]
print(new_list)
