# 1
my_list = [1, 0.1, True, 'string', [1,1]]

for el in my_list:
    print(type(el))

# 2
string = input('Please enter your list: ')
my_list = list(string)

#my_list = [1, 2, 3, 4, 5]
i = 0
first = 0
last = 0
for el in my_list:
    if (i % 2) == 0:
        first = el
    else:
        my_list[i] = first
        my_list[i-1] = el
    i += 1
print(my_list)

# 3.1
mon_list = ['jan', 'feb', 'march', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
index = int(input('Please enter your month: '))
if index > 12:
    print('Not correct data!')
    exit(-1);
for i, val in enumerate(mon_list):
    if index == (i + 1):
        print(val)

# 3.2
index = int(input('Please enter your month: '))
if index > 12:
    print('Not correct data!')
    exit(-1);

mon_dict = {1: 'jan', 2: 'feb', 3: 'march', 4: 'apr', 5: 'may', 6: 'jun', 7: 'jul', 8: 'aug', 9: 'sep', 10: 'oct',
            11: 'nov', 12: 'dec'}

for key, val in mon_dict.items():
    if index == key:
        print(val)

#4
words = input('Please enter your words: ')
list = words.split(' ')
for i, val in enumerate(list):
    if len(val) > 10:
        print("", i + 1, val[:10])
    else:
        print("",i+1, val)

#5
found = False
my_list = [7, 5, 3, 3, 2]
rating = int(input('Please enter your rating: '))
for i, val in enumerate(my_list):
    if val <= rating:
        my_list.insert(i, rating)
        found = True
        break
if not found:
    my_list.append(rating)
print(my_list)

#6
goods = [
    (1, {"название": "компьютер", "цена": 20000, "количество": 5, "eд": "шт."}),
    (2, {"название": "принтер", "цена": 6000, "количество": 2, "eд": "шт."}),
    (3, {"название": "сканер", "цена": 2000, "количество": 7, "eд": "шт."})
]

i = len(goods) + 1
while True:
    i += 1
    name = input("Введите название товара: ")
    price = input("Введите цену товара: ")
    count = input("Введите количество товара: ")
    unit = input("Введите единицу товара: ")
    goods.append((i, {"название": name, "цена": price, "количество": count, "eд": unit}))
    end = input("Хватит? ").lower()
    if end == 'yes' or end == 'да':
        break;

print(goods)

analytic = {}
keys = []
names = []
prices = []
counts = []
units = []

for good in goods:
    good = good[1]
    for key, val in good.items():
        if key == 'название':
            names.append(val)
        elif key == 'цена':
            prices.append(val)
        elif key == 'количество':
            counts.append(val)
        elif key == 'eд':
            units.append(val)

analytic = {
    "название": names,
    "цена": prices,
    "counts": counts,
    "units": units,
}

print(analytic)